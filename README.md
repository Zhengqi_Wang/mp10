# Rust Serverless Transformer Endpoint on AWS Lambda

This project demonstrates how to build, Dockerize, and deploy a Rust-based serverless transformer application using AWS Lambda. The application uses a Hugging Face transformer model and is set up to handle requests via AWS Lambda's serverless infrastructure.

## Prerequisites

- Rust and Cargo
- Docker
- AWS CLI configured with appropriate permissions
- Cargo Lambda

## Setup

### Step 1: Install Necessary Tools

Ensure Rust, Cargo, Docker, and AWS CLI are installed on your system. Install `cargo-lambda` to facilitate building AWS Lambda functions in Rust.

```bash
cargo install cargo-lambda
```

### Step 2: Initialize the Rust Project
Create a new project using `cargo-lambda`:

### Step 3: Develop the Application
Edit `main.rs` to include the logic for the transformer endpoint. This involves setting up the model and defining a handler that AWS Lambda will invoke.

### Step 4: Local Testing
Test the application locally using `cargo lambda watch` and use `curl` to send requests:
```bash
cargo lambda watch
curl "http://localhost:9000/default/transformer?text=example"
```

## Dockerization

### Step 1: Create a Dockerfile
Create a `Dockerfile` in the project root. This file should use an official Rust image, compile the Rust application, and set the compiled binary as the entry point.
```bash
FROM rust:1.58
WORKDIR /usr/src/app
COPY . .
RUN cargo build --release
CMD ["./target/release/transformer"]
```

![docker push](./Pictures/docker_push.png)

### Step 2: Build and Tag the Docker Image
First create a ECR for storage
![ECR](./Pictures/ECR.png)

Build the Docker image and tag it appropriately for your AWS ECR repository.
```bash
docker build -t 730335662926.dkr.ecr.us-east-2.amazonaws.com/mp10:latest .
```

### Step 3: Authenticate and Push to AWS ECR
Authenticate Docker with AWS ECR and push the Docker image.
```bash
aws ecr get-login-password --region us-east-2 | docker login --username AWS --password-stdin 730335662926.dkr.ecr.us-east-2.amazonaws.com
docker push 730335662926.dkr.ecr.us-east-2.amazonaws.com/mp10:latest
```


## Deployment on AWS Lambda
Create a Lambda function in the AWS Management Console using the Docker image pushed to ECR. Set up an API Gateway to route HTTP requests to your Lambda function

![lambda_image](./Pictures/lambda_image.png)

![lambda_function](./Pictures/lambda_function.png)


## Testing Deployment
Test the deployed Lambda function by sending HTTP requests to the API Gateway endpoint.

```bash
curl -X POST https://squcyscf5jzhhzz46e242qw6zy0pnwkv.lambda-url.us-east-2.on.aws/  -H "Content-Type: application/json" -d '{"text":"test"}'   
```

![](./Pictures/testing.png)